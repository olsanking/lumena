<?php
namespace App\Http\Controllers;

use App\Documents;
use App\Attachments;
use App\Previews;
use Illuminate\Http\Request;

class AttachmentsController extends Controller{
    //use Org_Heigl\Ghostscript\Ghostscript;
    //
    //    public function uploadAsFile(Request $request, $document){
    //
    //        /*
    //         *
    //         *  Does not work for Heroku
    //         *
    //         */
    //
    //        $file = Documents::findOrFail($document);
    //        $pdf = uniqid().'.pdf';
    //
    //        $destinationPath = 'uploads'.DIRECTORY_SEPARATOR.'pdf'.DIRECTORY_SEPARATOR;
    //        $bj = $request->file('pdf');
    //
    //        $bj->move($destinationPath, $pdf);
    //
    //        $file->attachment = $pdf;
    //        $file->save();
    //
    //        $attach = Attachments::create([
    //            'document'=> $document,
    //            'attachment'=> $bj->encode('pdf')
    //        ]);
    //        $attach->save();
    //
    //        return response()->json($destinationPath.$pdf, 201);
    //    }
    //    public function downloadAsFile($document){
    //        $file = Documents::findOrFail($document);
    //
    //        $pdf = $file->attachment;
    //        $destinationPath = 'uploads\\pdf\\';
    //        $headers = [
    //            'Content-Type' => 'application/pdf',
    //        ];
    //        return response()->download($destinationPath.$pdf, $file->name.'.pdf',$headers);
    //    }
    //
    //    public function preview(){
    //
    //        exec('gs -dSAFER -dPDFFitPage -sDEVICE=png16m -dJPEGQ=10 -dFirstPage=3 -dLastPage=5 -dTextAlphaBits=2 -dGraphicsAlphaBits=2 -g320x568 -r96 -o output.png Resume.pdf quit');
    //
    //        return response('output.png')
    //            ->header('Content-Type','image/png')
    //            ->header('Pragma','public')
    //            ->header('Content-Disposition','inline; filename="output.png"')
    //            ->header('Cache-Control','max-age=60, must-revalidate');
    //    }
    public function upload(Request $request, $document)
    {
        //Save Attachment and Previews
        Documents::findOrFail($document);

        $file = $request->pdf;
        if (strrpos($file, 'application')) {
            $attach = Attachments::create([
                'document' => $document,
                'attachment' => $file
            ]);
            $attach->save();
            return response()->json(
                [
                    'link'=>"documents/$document/attachment"
                ], 201
            );

        } elseif (strrpos($file,'image')) {

            $preview = Previews::create([
                'document'=> $document,
                'preview'=> $file
            ]);
            $preview->save();

            $count = Previews::where('document', $document)->groupBy('document')->count();

            return response()->json(
                [
                    "id"  => $preview->id,
                    "link"=> "documents/$document/attachment/preview/$count"
                ], 201
            );
        }
        return response()->json(['error'=>'Unsupported Media Type','data'=>$file,'document'=>$document], 415);
    }

    public function download($document){

        $file = Documents::findOrFail($document);

        $data = Attachments::where('document', $document)
                    ->orderBy('document', 'desc')
                    ->take(1)
                    ->get();

        $content = base64_decode(preg_replace('#^data:application/\w+;base64,#i', '', $data[0]->attachment));
        $output = 'temp/'.rand().'temp'.rand();
        file_put_contents($output, $content);

        if (file_exists($output)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/pdf');
            header('Content-Disposition: inline; filename="'.$file->name.'.pdf"');
            header('Expires: 31536000');
            header('Cache-Control: public, max-age=31536000');
            header('Pragma: public');
            header('Content-Length: '.filesize($output));
            readfile($output);
            exit;
        }
    }
}