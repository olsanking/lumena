<?php
namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    public function unsupported(){
        return response(view('unsupported'), 415);
    }

    public function home(){
        return response(view('home'), 200);
    }
}
