<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Previews;
use App\Attachments;
use Auth;

class DocumentsController extends Controller{
    public function __construct(){
        $this->middleware('auth');
    }

    public function createDocument(Request $request){
        /*
         * Only connection. Without file storage/ Files stored into Attachments
         */
        $this->validate($request, [
            'name' => 'required',
        ]);
        if ($file = Auth::user()->documents()->Create($request->all())) {
            return response()->json($file, 201);
        } else {
            return response()->json(['status' => 'fail']);
        }
    }

    public function deleteDocument($document){
        if (Auth::user()->documents()->findOrFail($document)->delete()) {
            Attachments::where('document', $document)->delete();
            Previews::where('document', $document)->delete();

            return response()->json(['status'=>'Removed successfully'], 204);
        } else {
            return response()->json(['status' => 'fail']);
        }
    }

    public function getDocument($document){
        $file = Auth::documents()->findOrFail($document);
        if (Auth::user()->documents()->findOrFail($document)) {
            return response()->json($file,  200);
        } else {
            return response()->json(['status' => 'fail']);
        }
    }

    public function all(){
        $documents  = Auth::user()->documents()->orderBy('id', 'ASC')->get();
        return response()->json($documents, 200);
    }
}