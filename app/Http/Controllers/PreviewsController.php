<?php
namespace App\Http\Controllers;

use App\Documents;
use App\Previews;

class PreviewsController extends Controller{
    public function preview($document, $view){
        $file = Documents::findOrFail($document);
        Previews::where('document', $document)->firstOrFail();

        $data = Previews::where('document', $document)
            ->orderBy('document', 'desc')
            ->get();

        $data = $data[$view]->preview;
        $output = 'temp/'.rand().'temp'.rand();
        $content = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $data));
        file_put_contents($output, $content);

        if (file_exists($output)) {
            header('Content-Description: File Transfer');
            header('Content-Type: image/png');
            header('Content-Disposition: inline; filename="'.$file->name.'_'.$view.'.png"');
            header('Expires: 31536000');
            header('Cache-Control: public, max-age=31536000');
            header('Pragma: public');
            header('Content-Length: '.filesize($output));
            readfile($output);
            exit;
        }
    }

    public function index($document){
        $previews = Previews::where('document', $document)->orderBy('id', 'asc')->get();
        return response()->json($previews, 200);
    }
}