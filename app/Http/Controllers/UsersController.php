<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Users;

class UsersController extends Controller
{
    public function token(Request $request)
    {
        $apikey = explode(' ',$request->header('Authorization'))[1];

        Users::where('api_key', $apikey)->firstOrFail();
        return response()->json(
            [
                'status' => 'success',
                'api_key' => $apikey
            ],
            202
        );
    }

    public function userForm()
    {
        return response(view('userForm'), 200);
    }

    public function authenticate(Request $request)
    {
        $this->validate(
            $request,
            [
                'email' => 'required',
                'password' => 'required'
            ]
        );

        $user = Users::where(
            'email',
            $request->input('email')
        )->first();

        if (
            Hash::check(
                $request->input('password'),
                $user->password
            )
        ) {
            $apikey = base64_encode(str_random(40));

            Users::where(
                'email',
                $request->input('email')
            )->update(
                [
                    'api_key' => $apikey
                ]
            );

            return response()->json(
                [
                    'status' => 'success',
                    'api_key' => $apikey
                ]
            );
        } else {
            return response()->json(['status' => 'fail'], 401);
        }
    }
}