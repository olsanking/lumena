<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Previews extends Model
{
 	protected $fillable = ['document', 'preview'];
}