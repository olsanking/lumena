<?php
namespace App;

use Illuminate\Auth\Authenticatable as AuthenticableTrait;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class Users extends Model implements Authenticatable
{
    use AuthenticableTrait;

    protected $fillable = ['email', 'password', 'api_key'];

    protected $hidden = [
        'password',
    ];

    public function documents()
    {
        return $this->hasMany('App\Documents', 'user_id');
    }
}
