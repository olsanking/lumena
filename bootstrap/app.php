<?php

require_once __DIR__.'/../vendor/autoload.php';

try {
    (new Dotenv\Dotenv(dirname(__DIR__)))->load();
} catch (Dotenv\Exception\InvalidPathException $e) {
    //
}

$app = new Laravel\Lumen\Application(
    dirname(__DIR__)
);

$app->withFacades();

$app->withEloquent();

$app->singleton(
    Illuminate\Contracts\Debug\ExceptionHandler::class,
    App\Exceptions\Handler::class
);

$app->singleton(
    Illuminate\Contracts\Console\Kernel::class,
    App\Console\Kernel::class
);

 $app->routeMiddleware([
     'auth' => App\Http\Middleware\Authenticate::class,
 ]); //for Auth

$app->register(App\Providers\AppServiceProvider::class); //for Auth
$app->register(App\Providers\AuthServiceProvider::class); //for Auth
// $app->register(App\Providers\EventServiceProvider::class);

$app->register(Clockwork\Support\Lumen\ClockworkServiceProvider::class); //for Auth

$app->routeMiddleware([
    'auth' => App\Http\Middleware\Authenticate::class,
]);

$app->router->group([
    'namespace' => 'App\Http\Controllers',
], function ($router) {
    require __DIR__.'/../routes/web.php';
});


return $app;