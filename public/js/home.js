const doc = ({id,name})=>
    `<div>
        <img src="api/v1/documents/${id}/attachment/previews/0">
        <b><a href="api/v1/documents/${id}/attachment">${name}</a></b><br>
        <a deleter href="api/v1/documents/${id}"><i class="fas fa-trash-alt"></i>Delete</a>
    </div>`;
const animate = state =>{

    if (state === true)
        document.body.setAttribute('animated','');
    else
        document.body.removeAttribute('animated');

};
const call = (action, type, f, method, data ) =>{

    animate(true);
    const headers = new Headers();
    if (localStorage.getItem('token')){

        headers.append('Authorization', 'salt '+ localStorage.getItem('token'));

    }

    return fetch(
        action,
        {
            method: method || 'GET',
            headers: headers,
            body: data||null
        }
    ).then(
        r=>r[type]()
    ).then(
        r=>{
            let exe = f(r);

            if (exe !== undefined)
                if ( exe.then )
                    exe.then(animate);
                else
                    animate();
            else
                animate();

            return exe;
        }
    ).catch(
        e=>
        {
            console.error('Lumen Call error',e);
            animate()
        }
    )

};

const upload = input=>{

    /*
    *
    * Upload PDF-file
    *
    * */

    console.log("upload: stat uploading");

    let documentId = input.dataset.document;
    let url = `api/v1/documents/${documentId}/attachment/upload`;
    let f =()=>{

        console.log("PDF uploaded");
        let container = input.parentNode.parentNode;
        let data = URL.createObjectURL( input.files[0] );
        container.setAttribute('hidden','');
        container.innerHTML = container.innerHTML;

        return savePreviews(data,url).then(update).then(animate);
    };

    return getPDF(
        input.files[0]
    ).then(
        pdfdata => {
            console.log("PDF Uploading ... ");
            let data = new FormData();
            data.append('pdf', pdfdata);
            call( url, 'text', f, 'POST', data );

        }
    );

};

const update = () =>{
    /*
    *
    *   Content reload
    *
    */
    let f = data=>{
        let s = '';
        if (data[0]){
            let docs = document.body.getElementsByClassName('documents')[0];
            for (let index in data) s += doc(data[index]);
            docs.innerHTML = s;
            while (docs.querySelectorAll('div').length % 20){
                docs.appendChild(document.createElement('div'));
            }

        }
        return s;
    };

    return call('api/v1/documents', 'json', f);
};
const formSubmit =e=> {

    e.stopPropagation();
    e.preventDefault();

    let form = e.target;
    let pdf = form.nextElementSibling;
    let f=({id, api_key})=>{
        /*
        *   Setup input into -> Get User file action
        */

        if (pdf){
            pdf.dataset.document = id;
            pdf.onchange=()=>upload(pdf);
        } else {
            localStorage.setItem('token',api_key);
            update();
            form.remove();
        }

    };

    call(form.action, 'json', f, form.method, new FormData(form));
    if (pdf) pdf.click();
    return false;
};

const slideUp =()=>{
    let docs = document.body.getElementsByClassName('documents')[0];
    for ( let i = 0; i < 20; i++)
        docs.appendChild(
            docs.removeChild(
                docs.querySelector('div')
            )
        );
};
const slideDown =()=>{
    let docs = document.body.getElementsByClassName('documents')[0];
    for ( let i = 0; i < 20; i++)
        docs.insertBefore(
            docs.removeChild(
                docs.querySelector('div:last-child')
            ),
            docs.querySelector('div:first-child')
        );
};

document.body.onwheel=e=>{

    if (e.deltaY > 0) slideUp();
    if (e.deltaY < 0) slideDown();

};
document.body.onload=update;
document.body.onsubmit=formSubmit;
document.body.onclick=e=>{

    let item=e.target;

    if (item.hasAttribute('deleter')){

        e.preventDefault();
        e.stopPropagation();

        if (confirm('Delete this document?'))
            call(item.href, 'text',()=>item.parentNode.remove(), 'DELETE');

    }

    if ( item.tagName === 'A' && item.dataset.target){

        e.preventDefault();
        e.stopPropagation();

        let f=html=>{
            document.getElementById(item.dataset.target).innerHTML = html;
        };
        call(item.href, 'text',f, 'GET');

        return false;
    }

};