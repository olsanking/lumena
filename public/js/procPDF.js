/*
     *   Previews by Mozila PDF.JS
     * */
function savePreviews(data_url, url ) {

    let page=0;
    console.log("Preview generation ");
    function getPDFImages() {

        console.log("At getPDFImages");
        __CURRENT_PAGE = page++;
        __PAGE_RENDERING_IN_PROGRESS = 1;

        let cnv = document.createElement('canvas');
        cnv.width = 100;
        return __PDF_DOC.getPage(
            page
        ).then(
            img=>{
                console.log("Get image");
                let scale = cnv.width / img.getViewport(1).width;
                let data = {
                    canvasContext: cnv.getContext('2d'),
                    viewport: img.getViewport(scale)
                };
                cnv.height = data.viewport.height;
                return img.render(data);
            }
        ).then(
            ()=>{
                console.log("Return Data URL");
                __PAGE_RENDERING_IN_PROGRESS = 0;
                return cnv.toDataURL();
            }
        );
    }

    let PDFHeart = pdfjsLib.getDocument(
        {
            url: data_url
        }
    ).then(
        file=>{
            __PDF_DOC = file;
            console.log("Before circle");
            for (let i=0;i < __PDF_DOC.numPages;i++){

                console.log("In circle");
                PDFHeart.then(
                    getPDFImages
                ).then(
                    imageData => new Promise(
                        resolve=>{
                            let t = performance.now();
                            console.log('Waiting');
                            while( performance.now() - t < 1000 ){}
                            console.log('OK');
                            resolve(imageData);
                        }
                    )
                ).then(
                    imageData=>{
                        console.log("Send Preview data",imageData);
                        let data = new FormData();
                        data.append('pdf', imageData);
                        call(url, 'text', e=>console.log(e), 'POST', data);
                    }
                );

            }
        }
    ).catch( error=> alert(error.message));

    return PDFHeart;

}

function getPDF(input){
    return new Promise(
        (resolve, reject) => {
            const reader = new FileReader();
            reader.readAsDataURL(input);
            reader.onload = () => resolve(reader.result);
            reader.onerror = reject;
        }
    );
};