<!DOCTYPE html>
<html lang="en">
<head>

    <title>Lumena PDF</title>
    <meta charset="UTF-8">

    <link href="/css/reset.css" rel="stylesheet">
    <link href="/css/user.css" rel="stylesheet">
    <link href="/css/documents.css" rel="stylesheet">
    <link href="/css/pdfs.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

    <script src="/js/pdf/build/pdf.js"></script>
    <!--[if lte IE 10]>
        <script type="text/javascript">document.location.href = '/unsupported-browser'</script>
    <![endif]-->

</head>
<body>
    <nav style="position:fixed; width: 100%; left:0; top:0;">
        <a href="api/v1/users" data-target="userForm">Login to preview your documents</a>
        <div id="userForm"></div>
    </nav>
    <header>
        <h1>Welcome to Lumena</h1>
        <br>
        <br>
        <div hidden class="createPDF">
            <div>
                <form action="api/v1/documents" method="post" >

                    <i class="fas fa-times" onclick="document.querySelector('.createPDF').setAttribute('hidden','')"></i>
                    <input name="name" required="required" placeholder="Type file name">
                    <label class="fas fa-arrow-right">
                        <input type="submit" hidden>
                    </label>

                </form>
                <input
                    hidden
                    name="pdf"
                    type="file"
                    multiple="false"
                    accept="application/pdf"
                >
            </div>
        </div>
        <button onclick="this.previousElementSibling.removeAttribute('hidden')"><i class="fas fa-upload"></i> add new document</button>
    </header>
    <section class="documents"></section>

    <script src="/js/procPDF.js"></script>
    <script src="/js/home.js"></script>

</body>
</html>