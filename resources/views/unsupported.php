<!DOCTYPE html>
<html>
    <head>
        <style>
            html{
                display: flex;
                align-items: center;
                text-align: center;
                width:100%;
                height:100%;
            }
            body{
                margin: auto ;
                width: 60%;
            }
            img{
                height: 4em;
            }
        </style>
    </head>
    <body>
        <h1>Unsupported browser</h1>
        <p>
            This website does not support your browser.
            </br>
            Please use modern browser instead
            <br>
            <br>
            <a href="https://www.google.com/chrome/"><img src="https://www.chromium.org/_/rsrc/1438811752264/chromium-projects/logo_chrome_color_1x_web_32dp.png"></a>
            <a href="https://www.mozilla.org/uk/firefox/"><img src="https://www.mozilla.org/media/img/logos/firefox/logo-quantum.9c5e96634f92.png"></a>
            <a href="https://www.microsoft.com/uk-ua/windows/microsoft-edge"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Microsoft_Edge_logo.svg/96px-Microsoft_Edge_logo.svg.png"></a>
            <a href="https://www.opera.com"><img src="https://img.comss.net/fit-in/200x200/filters:fill(FFFFFF)/logo/opera-new.png"></a>
        </p>
    </body>
</html>