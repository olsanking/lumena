<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
$router->get('/', 'Controller@home');
$router->get('unsupported-browser', 'Controller@unsupported');
$router->group(['prefix' => 'api/v1'], function ($router) {
    $router->group(['prefix' => 'documents'], function($router) {
        $router->get('/','DocumentsController@all'); //Get all documents
        $router->post('/','DocumentsController@createDocument'); //Add new one
        $router->group(['prefix' => '{document}'], function($router) {
            $router->get('/', 'DocumentsController@getDocument'); //Get document data
            $router->delete('/','DocumentsController@deleteDocument'); //Delete
            $router->group(['prefix' => 'attachment'], function($router) {
                $router->get('/', 'AttachmentsController@download'); //get PDF from server
                $router->group(['prefix' => 'upload'], function($router) {
                    $router->get('/', ''); //get form for uploading?
                    $router->post('/','AttachmentsController@upload'); //send PDF to server
                });
                $router->group(['prefix' => 'previews'], function($router) {
                    $router->get('/', 'PreviewsController@index'); //get all previews links(id=>link,...)
                    $router->get('{preview}','PreviewsController@preview'); //get preview image file
                });
            });
        });
    });

    $router->post('auth/token', 'UsersController@token');

    $router->group(['prefix' => 'users'], function($router) {
        $router->get('/','UsersController@userForm'); //Get auth form
        $router->post('/','UsersController@authenticate'); //Login in
    });
});